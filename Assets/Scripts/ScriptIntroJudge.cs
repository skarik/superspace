﻿using UnityEngine;
using System.Collections;

public class ScriptIntroJudge : MonoBehaviour
{
    [System.Serializable]
    enum Action
    {
        ProceedWithLine,
        ShowYesNo,
        ShowYesYes,
        ShowGuiltyNotGuilty,
        DropPlayer,
        Ending,
    }

    [System.Serializable]
    class ActionInfo_t
    {
        public Action type;
        public AudioClip audioLine;
        public string title;
        public string subtitle;
    }
    [SerializeField]
    ActionInfo_t[] m_actions;

    int _state = -1;
    bool _lockPlayer = true;
    Transform _playerRoot;

    Transform _buttonYes_Parent;
    Transform _buttonNo_Parent;
    ButtonBase _buttonYes;
    ButtonBase _buttonNo;
    TextMesh _buttonYesText;
    TextMesh _buttonNoText;
    AudioSource _currentAudio;

    bool _updated = false;
    float _buttonHideDelayTime = 0;
    bool _buttonHidden = false;

    void Start()
    {
        _playerRoot = GameObject.Find("Player").transform.Find("Vive Objects");

        _buttonYes_Parent = transform.Find("Yes");
        _buttonNo_Parent = transform.Find("No");
        _buttonYes = _buttonYes_Parent.Find("back").Find("front").GetComponent<ButtonBase>();
        _buttonNo = _buttonNo_Parent.Find("back").Find("front").GetComponent<ButtonBase>();
        _buttonYesText = _buttonYes.transform.Find("text").GetComponent<TextMesh>();
        _buttonNoText = _buttonNo.transform.Find("text").GetComponent<TextMesh>();
    }

    void Update()
    {
        if (_state >= 0)
        {
            switch (m_actions[_state].type)
            {
                case Action.ProceedWithLine:
                    // Do line
                    if (_currentAudio != null)
                    {
                        if (_updated == false && _currentAudio.time > _currentAudio.clip.length * 0.35F)
                        {
                            UpdateText();
                            _updated = true;
                        }
                        if (!_currentAudio.isPlaying) // We're done
                        {
                            Proceed();
                        }
                    }
                    else
                    {
                        Proceed();
                    }
                    // Do button hide delay
                    _buttonHideDelayTime += Time.deltaTime;
                    if (_buttonHideDelayTime > 0.2F && !_buttonHidden)
                    {
                        _buttonHidden = true;
                        _buttonYes_Parent.gameObject.SetActive(false);
                        _buttonNo_Parent.gameObject.SetActive(false);
                    }
                    break;
            }
        }
    }

    void FixedUpdate()
    {
        if (_lockPlayer )
        {
            _playerRoot.transform.position = Vector3.zero;
            _playerRoot.transform.rotation = Quaternion.identity;
            _playerRoot.GetComponent<Rigidbody>().velocity = Vector3.zero;
            _playerRoot.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
        else
        {

        }
    }

    public void Proceed ( )
    {
        _updated = false;
        _buttonHideDelayTime = 0.0F;
        _buttonHidden = false;
        _currentAudio = null;

        _state += 1;
        switch (m_actions[_state].type)
        {
            case Action.ProceedWithLine:
                GameObject audio = SoundPlayer.playOneShot(m_actions[_state].audioLine, transform.Find("Title").position + new Vector3(0,0,25));
                _currentAudio = audio.GetComponent<AudioSource>();
                break;
            case Action.ShowGuiltyNotGuilty:
                UpdateText();
                _buttonYes.Reset();
                _buttonNo.Reset();
                _buttonYes_Parent.gameObject.SetActive(true);
                _buttonNo_Parent.gameObject.SetActive(true);
                _buttonYesText.text = "Guilty";
                _buttonNoText.text = "Not Guilty";
                break;
            case Action.ShowYesNo:
                UpdateText();
                _buttonYes.Reset();
                _buttonNo.Reset();
                _buttonYes_Parent.gameObject.SetActive(true);
                _buttonNo_Parent.gameObject.SetActive(true);
                _buttonYesText.text = "Yes";
                _buttonNoText.text = "No";
                break;
            case Action.ShowYesYes:
                UpdateText();
                _buttonYes.Reset();
                _buttonNo.Reset();
                _buttonYes_Parent.gameObject.SetActive(true);
                _buttonNo_Parent.gameObject.SetActive(true);
                _buttonYesText.text = "Yes";
                _buttonNoText.text = "Yes";
                break;
            case Action.DropPlayer:
                _lockPlayer = false;
                _playerRoot.GetComponent<Rigidbody>().useGravity = true;
                break;
        }
    }


    public void UpdateText()
    {
        TextMesh title = transform.Find("Title").GetComponent<TextMesh>();
        TextMesh subtitle = transform.Find("Subtitle").GetComponent<TextMesh>();

        if ( m_actions[_state].title == "\\clear" || m_actions[_state].title == "/clear")
        {
            title.text = "";
        }
        else if (m_actions[_state].title != "" )
        {
            title.text = m_actions[_state].title;
        }

        if (m_actions[_state].subtitle == "\\clear" || m_actions[_state].subtitle == "/clear")
        {
            subtitle.text = "";
        }
        else if (m_actions[_state].subtitle != "")
        {
            subtitle.text = m_actions[_state].subtitle;
        }
    }
}
