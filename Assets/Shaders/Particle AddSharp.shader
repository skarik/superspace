Shader "Particles/Additive Alpha Test" {
Properties {
	_Color ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_InvFade ("Soft Particles Factor", Range(0.01,3.0)) = 1.0
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha One
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _Color;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				#ifdef SOFTPARTICLES_ON
				float4 projPos : TEXCOORD2;
				#endif
			};
			
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				#ifdef SOFTPARTICLES_ON
				o.projPos = ComputeScreenPos (o.vertex);
				COMPUTE_EYEDEPTH(o.projPos.z);
				#endif
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			sampler2D_float _CameraDepthTexture;
			float _InvFade;
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 color = tex2D(_MainTex, i.texcoord);
				// TODO: make this shit based on vector-encoded images, not blur (can get some more speed then)
				color.a +=
					tex2D(_MainTex, i.texcoord - float2(_MainTex_TexelSize.x,0) ).a +
					tex2D(_MainTex, i.texcoord + float2(_MainTex_TexelSize.x,0) ).a +
					tex2D(_MainTex, i.texcoord - float2(0,_MainTex_TexelSize.y) ).a +
					tex2D(_MainTex, i.texcoord + float2(0,_MainTex_TexelSize.y) ).a;
				color.a /= 5.0;
				// Perform alpha-testing
				clip( color.a - 0.5f );
				
				UNITY_APPLY_FOG_COLOR(i.fogCoord, color, fixed4(0,0,0,0)); // fog towards black due to our blend mode
				return color * 2.0F * _Color;
			}
			ENDCG 
		}
	}	
}
}
