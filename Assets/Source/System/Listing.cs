﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Singleton for the lazy! Meant to be used as a single listing to repeatedly grabbed Objects.
/// 
/// </summary>
public class Listing : MonoBehaviour
{
    public AudioClip soundDoorOpen;
    public UnityEngine.Audio.AudioMixerGroup outputMixerGroup;


    // ========================================================================
    // Do not edit below this point.
    // ========================================================================

    /// <summary>
    /// Get the currently active listing.
    /// </summary>
    public static Listing Active { get { return _active; } }

    static Listing _active = null;
    public void Awake()
    {
        if (_active != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _active = this;
        }
    }
}
