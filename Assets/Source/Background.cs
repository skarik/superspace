﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour
{
    public Camera _main_camera;
	
	// Update is called once per frame
	void LateUpdate ()
    {
        transform.position = _main_camera.transform.position;
        transform.localScale = Vector3.one * _main_camera.farClipPlane / 3.0F;
    }
}
