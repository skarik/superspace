﻿using UnityEngine;
using System.Collections;

public class TestGun : ItemHandyBase
{
    public Transform m_cameraRig;
    public GameObject m_ballShot;

    float _shotChargetime = 0.0F;
    const float ShotChargeStartTime = 1.0F;
    float _shotChargeStrength = 0.0F;

    GameObject _ballChargeShot;
    Transform _shotPoint;
    Transform _projectionObject;
    Transform _intermediate;

    enum Mode
    {
        Cutter = 0,
        Thruster,
        Beam,
        Grapple
    }
    Mode _mode = Mode.Cutter;

    new void Start()
    {
        base.Start();
        Transform start_transform = transform;
        _intermediate = transform.FindChild("Body");
        if (_intermediate != null) start_transform = _intermediate;
        _shotPoint = start_transform.FindChild("ShotPoint");
        _projectionObject = start_transform.FindChild("Projection");
    }

    new void Update()
    {
        base.Update();

        if (_device.DiscoverController())
        {
            if (_mode == Mode.Beam)
                UpdateBeamShot();
            else if (_mode == Mode.Thruster)
                UpdateThruster();
            else if (_mode == Mode.Cutter)
                UpdateCutter();

            if (_mode != Mode.Cutter)
                EndCutter();
        }

        // Update projection UI help
        if (_mode == Mode.Thruster)
        {
            _projectionObject.transform.rotation = Quaternion.FromToRotation(Vector3.forward, MotionPropelNormal);
            _projectionObject.gameObject.SetActive(true);
        }
        else if ( _mode == Mode.Beam || _mode == Mode.Grapple )
        {
            _projectionObject.transform.rotation = Quaternion.FromToRotation(Vector3.forward, _shotPoint.transform.forward);
            _projectionObject.gameObject.SetActive(true);
        }
        else
        {
            _projectionObject.gameObject.SetActive(false);
        }
    }

    void UpdateBeamShot ( )
    {
        if (_device.Controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            FireQuickshot();
        }
        if (_device.Controller.GetPress(SteamVR_Controller.ButtonMask.Trigger))
        {
            _shotChargetime += Time.deltaTime;
            if (_shotChargetime > ShotChargeStartTime)
            {
                TriggerHapticFeedback(
                    Mathf.Min(3500.0F, (_shotChargetime - ShotChargeStartTime) * 1500.0F + 50.0F),
                    Mathf.Min(_shotChargetime, 8.0F)
                    );

                if (_ballChargeShot == null)
                {
                    _ballChargeShot = (GameObject)Instantiate(m_ballShot, _shotPoint.transform.position, _shotPoint.transform.rotation);
                    _ballChargeShot.transform.parent = _shotPoint;
                }
                _shotChargeStrength = 1.0F + 1.5F * Mathf.Min(_shotChargetime - ShotChargeStartTime, 2.0F);
                _ballChargeShot.transform.localScale = Vector3.one * _shotChargeStrength;

                // Push the player
                PushPlayer(-5.0F * _shotChargeStrength);
            }
        }
        else // Button released
        {
            if (_shotChargetime > ShotChargeStartTime)
            {
                // Do another shot
                ShotBallMotion shotBehavior = _ballChargeShot.GetComponent<ShotBallMotion>();
                shotBehavior.enabled = true;
                shotBehavior.Motion = _shotPoint.forward * 20.0F;
                _ballChargeShot.transform.parent = null;
                _ballChargeShot = null;
            }
            _shotChargetime = 0.0F;
        }
    }
    void UpdateThruster()
    {
        Destroy(_ballChargeShot);
        float strength = _device.Controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
        // if (_device.Controller.GetPress(SteamVR_Controller.ButtonMask.Trigger))
        if (strength > 0.15F)
        {
            FireThruster(strength);
        }
    }

    Light _cutterLight;
    void UpdateCutter()
    {
        if (_cutterLight == null)
        {
            GameObject cutter_gameobject = new GameObject("Cutter Light");
            cutter_gameobject.transform.parent = _shotPoint;
            cutter_gameobject.transform.localPosition = Vector3.zero;
            _cutterLight = cutter_gameobject.AddComponent<Light>();
            _cutterLight.enabled = false;
            _cutterLight.shadows = LightShadows.Soft;
        }

        float strength = _device.Controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
        if (_device.Controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {   // Ignite on down
            _cutterLight.enabled = true;
            TriggerHapticFeedback(1000, 3);
        }
        if ( strength < 0.15F )
        {   // Deignite on release
            _cutterLight.enabled = false;
        }
        else
        {   // Otherwise we can vary it
            _cutterLight.range = 5.0F * strength;
            _cutterLight.intensity = 1.5F * strength;
            _cutterLight.color = Color.Lerp(_cutterLight.color, Color.HSVToRGB(Random.Range(0.1F, 0.15F), Random.Range(0.15F, 0.25F), 1.0F), 0.1F); // TODO: Remove lerp
        }
        
    }
    void EndCutter()
    {
        if (_cutterLight != null)
        {
            Destroy(_cutterLight.gameObject);
        }
    }


    Vector3 MotionPropelNormal
    {
        get
        {
            Vector3 aiming_dir = _shotPoint.transform.forward;
            aiming_dir += m_cameraRig.forward * Mathf.Max( 0.0F, Vector3.Dot(aiming_dir, m_cameraRig.forward)) * 2.0F;
            return (aiming_dir).normalized;
        }
    }

    void FireQuickshot( )
    {
        TriggerHapticFeedback(1000, 3);

        GameObject shot = (GameObject)Instantiate(m_ballShot,_shotPoint.transform.position,_shotPoint.transform.rotation);
        ShotBallMotion shotBehavior = shot.GetComponent<ShotBallMotion>();
        shotBehavior.enabled = true;
        shotBehavior.Motion = _shotPoint.forward * 20.0F;

        PushPlayer(-200.0F);
    }
    void FireThruster (float percent)
    {
        TriggerHapticFeedback(1000 * percent, 3);

        PushPlayer(100.0F * percent);
    }

    void PushPlayer(float strength)
    {
        if (_affectedBody != null)
        {
            Vector3 applied_position = transform.position - m_cameraRig.transform.position;
            applied_position += _affectedBody.transform.position;
            _affectedBody.AddForceAtPosition(MotionPropelNormal * strength * 0.001F, applied_position);
            _affectedBody.AddForce(MotionPropelNormal * strength);
        }
    }

    public override string GetModeNameShort(int mode)
    {
        switch (mode)
        {
            case 0: return "CUT";
            case 1: return "PSH";
            case 2: return "BEAM";
            case 3: return "GRPL";
        }
        return "???";
    }
    public override string GetModeName(int mode)
    {
        switch (mode)
        {
            case 0: return "Cutter Torch";
            case 1: return "Thrust Module";
            case 2: return "Beam Shot";
            case 3: return "Grapple Tool";
        }
        return "Unknown";
    }
    public override bool GetModeEnabled(int mode)
    {
        return true;
    }
    public override void SetMode(int mode)
    {
        _mode = (Mode)mode;
    }



    /*public void OnCollisionEnter ( Collision collision )
    {
        foreach ( ContactPoint contact in collision.contacts )
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
            _affectedBody.
        }
    }*/
    Vector3 _previous_position;
    Vector3 _current_velocity;

    void FixedUpdate()
    {
        // Update player control velocity
        {
            Vector3 delta = transform.position - _previous_position;
            _previous_position = transform.position;

            _current_velocity = Vector3.Lerp(
                _current_velocity,
                (delta / Time.deltaTime) - _affectedBody.velocity,
                0.5F * (0.05F + delta.magnitude));
        }

        // Check for pushing against walls
        if (_intermediate != null)
        {
            Vector3 delta = _intermediate.position - transform.position;
            if (delta.magnitude > 0.001F)
            {
                _affectedBody.MovePosition(_affectedBody.position + delta);
                //_affectedBody.velocity += delta;
                //_affectedBody.AddForce(delta);
                RaycastHit hitInfo;
                if ( Physics.Raycast(new Ray(_intermediate.position, -delta), out hitInfo, delta.magnitude, 1 << 9 ) )
                {   // Need to directly change velocity to fix future shitcases
                    _affectedBody.velocity += hitInfo.normal * -Vector3.Dot(_affectedBody.velocity, hitInfo.normal);
                    _affectedBody.velocity += _current_velocity.magnitude * hitInfo.normal;
                }
            }
        }
    }
}
