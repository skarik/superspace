﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{
    PlayerObject _player;

    // grab all references to objects (like the player and stuff)
    private void GrabReferences()
    {
        if (_player == null)
            _player = PlayerObject.Active;
    }

    // Use this for initialization
    void Start()
    {
        GrabReferences();
    }

    // Update is called once per frame
    void Update()
    {
        GrabReferences();
        if (_player != null)
            transform.position = _player.transform.position;
    }
}