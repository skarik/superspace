﻿using UnityEngine;
using System.Collections;

public class KillOnSoundEnd : MonoBehaviour {

    AudioSource m_audio;

	// Use this for initialization
	void Start () {
        m_audio = GetComponent<AudioSource>();
        if (!m_audio)
        {
            Destroy(this);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!m_audio.isPlaying)
        {
            Destroy(this.gameObject);
        }
	}
}
