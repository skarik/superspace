﻿using UnityEngine;
using System.Collections;

public class SoundPlayer
{
    public static GameObject playOneShot(AudioClip n_clip, Vector3 position, Transform n_parent = null)
    {
        AudioSource src;
        return playOneShot(n_clip, position, n_parent, out src);
    }

    public static GameObject playOneShot(AudioClip n_clip, Vector3 position, Transform n_parent, out AudioSource o_source)
    {
        GameObject obj = new GameObject();
        obj.transform.position = position;
        obj.transform.parent = n_parent;
        obj.name = "Oneshot Sound";
        AudioSource source = obj.AddComponent<AudioSource>();
        source.clip = n_clip;
        source.spatialBlend = 1.0f;
        source.maxDistance = 50.0f;
        source.minDistance = 6.0f;
        source.rolloffMode = AudioRolloffMode.Logarithmic;
        source.dopplerLevel = 0.5f;
        source.spread = 90.0f;
        source.volume = 1.0f;
        source.pitch = 1.0f;
        source.Play();
        source.outputAudioMixerGroup = Listing.Active.outputMixerGroup;
        o_source = source;
        obj.AddComponent<KillOnSoundEnd>();
        return obj;
    }
}
