﻿using UnityEngine;
using System.Collections;

public class PlayerObject : MonoBehaviour
{
    static PlayerObject _activePlayer;
    public static PlayerObject Active { get { return _activePlayer; } }
    protected void Start()
    {
        _activePlayer = this;
    }
}