﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ButtonBase : MonoBehaviour
{
    public UnityEvent m_onFirstTouch;
    public UnityEvent m_onDepress;

    public bool m_useOnce = false;
    public bool m_isToggle = false;
    public bool m_depressOnUse = true;

    bool _pressed_on = false;

    Transform _front;
    Transform _back;
    Collider _collider;

    Renderer _front_renderer;
    Renderer _back_renderer;
    Color _front_renderer_baseline;
    Color _back_renderer_baseline;

    float _float_distance;
    float _current_button_distance;
    float _current_button_distance_previous;
    float _target_button_distance;

    bool _reset_enabled = true;
    float _reset_timer = 0.0F;

    ItemHandyBase _lastItem;

    void Start()
    {
        //_front = transform.Find("front");
        //_back = transform.Find("back");
        _front = transform;
        _back = transform.parent;
        if (_front == null || _back == null)
            Debug.LogError("Invalid button setup!");

        _collider = _front.GetComponent<Collider>();

        _front_renderer = _front.GetComponent<Renderer>();
        _back_renderer = _back.GetComponent<Renderer>();
        if (_front_renderer)
            _front_renderer_baseline = _front_renderer.material.color;
        if (_back_renderer)
            _back_renderer_baseline = _back_renderer.material.color;

        _float_distance = _front.localPosition.y;
        _current_button_distance = _float_distance;
        _current_button_distance_previous = _float_distance;
        _target_button_distance = _float_distance;
    }

    public void Reset ()
    {
        _pressed_on = false;

        _reset_enabled = true;
        _reset_timer = 0.0F;
    }

    void Update()
    {
        // Check for press logic
        if (_current_button_distance_previous > 0 && _current_button_distance <= 0 )
        {
            if (_lastItem)
            {
                _lastItem.Device.Controller.TriggerHapticPulse(500);
            }
            m_onDepress.Invoke();

            // Change the state on pressed down
            if (m_isToggle)
                _pressed_on = !_pressed_on;
            else
                _pressed_on = true;
        }
        else if (_current_button_distance_previous < 0 && _current_button_distance >= 0 )
        {
            // Change state when button released
            if (!m_useOnce && !m_isToggle)
            {
                _pressed_on = false;
            }
        }
        _current_button_distance_previous = _current_button_distance;

        // Change renderer colors if enabled
        if (_front_renderer)
        {
            if (_pressed_on)
            {
                _front_renderer.material.color = _front_renderer_baseline * 1.8F;
            }
            else
            {
                _front_renderer.material.color = _front_renderer_baseline;
            }
        }

        // Perform position reset
        if (_reset_enabled)
        {
            _reset_timer += Time.deltaTime;
            if (_reset_timer > 0.05F)
            {
                if (m_depressOnUse && _pressed_on)
                    _target_button_distance = _float_distance * 0.05F; // Set to 5% of height if a stay-press button
                else
                    _target_button_distance = _float_distance;
            }
        }
    }

    void FixedUpdate()
    {
        // Lerp faster when going down
        if (_target_button_distance < _current_button_distance)
            _current_button_distance = _target_button_distance;
        else
            _current_button_distance = Mathf.Lerp(_current_button_distance, _target_button_distance, 0.25F);
        // Set position
        _front.localPosition = new Vector3(0, _current_button_distance, 0);
    }

    void OnCollisionEnter(Collision collision)
    {
        ItemHandyBase item = GetHandyObject(collision.rigidbody);
        if (item != null)
        {
            //item.TriggerHapticFeedback(1000, 0.1F);
            if (_reset_enabled)
                item.Device.Controller.TriggerHapticPulse(500);
            _lastItem = item;
        }
        m_onFirstTouch.Invoke();

        OnCollisionStay(collision);
    }

    void OnCollisionStay(Collision collision)
    {
        bool had_collision = false;
        float min_y = _target_button_distance;
        foreach (ContactPoint contact in collision.contacts)
        {
            // Get normal of collision
            Vector3 localOffset = _back.InverseTransformVector(contact.normal * Mathf.Abs(contact.separation));

            if (localOffset.y < 0)
            {
                // Find collision point
                Vector3 localHitPoint = _back.InverseTransformPoint(contact.point);
                //Vector3 localHitPoint = collision.contacts[0].point;
                float next_y = localHitPoint.y - Mathf.Abs(localOffset.y) * 1.1F;
                if (next_y < min_y)
                    min_y = next_y;
                //Debug.Log(localHitPoint);
                //Debug.Log(Mathf.Abs(localOffset.y));
                had_collision = true;
                //Debug.DrawLine(transform.position, contact.point);
            }
        }
        if (had_collision)
        {
            _target_button_distance = min_y;
            _reset_enabled = false;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        _reset_timer = 0.0F;
        _reset_enabled = true;
    }
    void OnTriggerExit(Collider other)
    {
        _reset_timer = 0.0F;
        _reset_enabled = true;
    }

    protected ItemHandyBase GetHandyObject(Rigidbody other)
    {
        if (other.transform.parent != null)
        {
            return other.transform.parent.GetComponent<ItemHandyBase>();

        }
        return null;
    }
}