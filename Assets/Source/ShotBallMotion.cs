﻿using UnityEngine;
using System.Collections;

public class ShotBallMotion : MonoBehaviour
{
    public GameObject m_hit_effect;

    Vector3 _motion;
    public Vector3 Motion
    {
        set
        {
            _motion = value;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(transform.position, _motion);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, _motion.magnitude * Time.deltaTime) )
        {
            Quaternion hit_rotation = Quaternion.FromToRotation(Vector3.forward, hitInfo.normal);
            GameObject hit_effect = (GameObject)GameObject.Instantiate(m_hit_effect, hitInfo.point + hitInfo.normal * 0.01F, hit_rotation);
            hit_effect.transform.parent = hitInfo.collider.gameObject.transform;
            if (hitInfo.rigidbody != null)
            {
                hitInfo.rigidbody.AddForceAtPosition(Vector3.forward * 200.0F, transform.position);
            }
            Destroy(this.gameObject);
        }
        else
        {
            transform.position += _motion * Time.deltaTime;
        }
    }
}