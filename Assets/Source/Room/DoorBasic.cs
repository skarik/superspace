﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DoorBasic : MonoBehaviour
{
    public string m_name = "Shitty door";
    public DoorBasic m_target;

    // keep a list of all the doors
    static List<DoorBasic> m_doorList = new List<DoorBasic>();
    void Awake()
    {
        m_doorList.Add(this);
    }
    void OnDestroy()
    {
        m_doorList.Remove(this);
    }

    // Use this for initialization
    void Start()
    {
        GrabReferences();
        // set up room
        if (transform.parent != transform.root && transform.parent != null && transform.parent.gameObject != null && transform.parent.gameObject.activeInHierarchy)
        {
            if (transform.parent.gameObject.GetComponent<RoomBehavior>() == null)
            {
                transform.parent.gameObject.AddComponent<RoomBehavior>();
            }
        }
        // Set up door reference
        if (m_target != null && m_target.m_target != this )
        {
            if (m_target.m_target == null)
            {
                Debug.Log("One-way null door target found. Helping a brother out and linking them properly.");
                m_target.m_target = this;
            }
            else
            {
                Debug.LogError("Invalid target set for door " + this);
            }
        }
    }

    PlayerObject _player;
    Animation _animation;
    bool _isOpen = false;
    bool _isClosing = false;

    // grab all references to objects (like the player and stuff)
    private void GrabReferences()
    {
        if (_player == null)
            _player = PlayerObject.Active;
        if (_animation == null)
            _animation = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        GrabReferences();
        if (_player == null) return;

        // open door when player is close
        if ((_player.transform.position - transform.position).magnitude < 3.0f)
        {
            OpenDoor();
        }

        // close door if player is far away
        if (_isOpen)
        {
            if ((_player.transform.position - transform.position).magnitude > 6.0f)
            {
                // close the door
                CloseDoor();

                if (_isClosing && (_animation["open"].time <= 0.001f))
                {
                    _isOpen = false;
                    _isClosing = false;

                    RoomBehavior.HideUnused();
                }
            }
        }
    }

    public void OpenDoor()
    {
        GrabReferences();

        if (m_target != null && m_target != this && (_isOpen == false || _isClosing == true))
        {
            SoundPlayer.playOneShot(Listing.Active.soundDoorOpen, transform.position);

            // mark door as open
            _isOpen = true;
            _isClosing = false;

            // play animation
            _animation["open"].time = Mathf.Clamp(_animation["open"].time, 0.0F, _animation["open"].length);
            _animation["open"].speed = 1.0f;
            _animation["open"].wrapMode = WrapMode.ClampForever;
            _animation.Play("open");

            // find the target room and open that shit
            Transform targetRoom = m_target.transform.parent;
            if (targetRoom != null)
            {
                //m_target.transform.position = transform.position;
                targetRoom.transform.rotation *= transform.rotation * Quaternion.Inverse(Quaternion.Euler(0,180,0) * m_target.transform.rotation);

                targetRoom.position += transform.position - m_target.transform.position;
                //targetRoom.gameObject.SetActive(true);
                targetRoom.GetComponent<RoomBehavior>().ShowRoom();
            }
            m_target.MatchOpenState();
        }
    }
    public void MatchOpenState()
    {
        GrabReferences();

        // mark door as open
        _isOpen = true;
        _isClosing = false;

        // play animation
        _animation["open"].time = Mathf.Clamp(_animation["open"].time, 0.0F, _animation["open"].length);
        _animation["open"].speed = 1.0f;
        _animation["open"].wrapMode = WrapMode.ClampForever;
        _animation.Play("open");
    }

    public void CloseDoor()
    {
        GrabReferences();

        if (_isOpen == true && _isClosing != true)
        {
            _isClosing = true;

            // close the door
            _animation["open"].time = Mathf.Clamp(_animation["open"].time, 0.0F, _animation["open"].length);
            _animation["open"].speed = -1.0f;
            _animation["open"].wrapMode = WrapMode.ClampForever;
            _animation.Play("open");
        }
    }
}