﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomBehavior : MonoBehaviour
{
    public enum Environment
    {
        Space,
        Station,
        StationNoAir
    };


    public bool m_startHidden = true;
    public List<RoomBehavior> m_visibleRooms;
    public Environment m_environment = Environment.Station;

    // keep a list of all the rooms
    static List<RoomBehavior> m_roomList = new List<RoomBehavior>();
    void Awake()
    {
        m_roomList.Add(this);
    }
    void OnDestroy()
    {
        m_roomList.Remove(this);
    }
    static RoomBehavior _activeRoom = null;
    static PlayerObject _player;

    // Use this for initialization
    void Start()
    {
        if (m_startHidden)
        {
            transform.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_player == null)
            _player = FindObjectOfType<PlayerObject>();

        if (_player != null)
        {
            CompareRooms();
        }

        if (_activeRoom == this)
        {
            //Listing.Active.outputMixerGroup.audioMixer.SetFloat("LowpassFrequency", 200.0f);
            // TODO: Fix this
        }
    }

    protected Bounds _bounds;

    public void ShowRoom()
    {
        transform.gameObject.SetActive(true);

        _bounds = new Bounds(transform.position, Vector3.zero);
        AddChildrenToBounds(transform);
    }
    public void HideRoom()
    {
        transform.gameObject.SetActive(false);
    }

    public static void HideUnused()
    {
        foreach (RoomBehavior room in m_roomList)
        {
            if (room != _activeRoom)
                room.HideRoom();
        }
    }

    protected void AddChildrenToBounds(Transform target)
    {
        // make the room contain the object
        Renderer renderer = target.GetComponent<Renderer>();
        if (renderer != null)
        {
            _bounds.Encapsulate(renderer.bounds);
        }
        else
        {
            _bounds.Encapsulate(target.position);
        }
        // add the object's children
        foreach (Transform child in target)
        {
            AddChildrenToBounds(child);
        }
    }

    protected void CompareRooms()
    {
        // Skip this check if already the active room
        if (_activeRoom == this)
            return;
        //  No active room? Set active room.
        if (_activeRoom == null)
            _activeRoom = this;
        // Active room isn't active?
        else if (!_activeRoom.gameObject.activeInHierarchy)
        {   // Activate this one instead
            _activeRoom = this;
        }
        // This room contains the player?
        else if (_bounds.Contains(_player.transform.position))
        {   
            // Active room doesn't? Well use this one!
            if (!_activeRoom._bounds.Contains(_player.transform.position))
                _activeRoom = this;
            else
            {   // Real check when rooms overlap.
                Vector3 delta_them = _activeRoom._bounds.center - _player.transform.position;
                Vector3 delta_me = _bounds.center - _player.transform.position;
                // If the existing room is farther than this room is, use this room
                if (delta_them.sqrMagnitude > delta_me.sqrMagnitude)
                    _activeRoom = this;
            }
        }
    }
}
