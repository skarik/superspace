﻿using UnityEngine;
using System.Collections;

public class TestPlayer : PlayerObject
{
    //public TestGun m_gunObject;
    public ItemHandyBase[] m_items;
    public SteamVR_TrackedObject m_cameraObject;

    SteamVR_ControllerManager _controllerManager;
    Rigidbody _rigidbody;
    ViveDevice _headDevice;
    Renderer _viewgrid;

    float _viewgridStrength = 2.0F;

    // Use this for initialization
    protected new void Start()
    {
        base.Start();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = true;
        //m_gunObject.AffectedBody = _rigidbody;
        for ( int i = 0; i < m_items.Length; ++i )
        {
            m_items[i].AffectedBody = _rigidbody;
        }

        _headDevice = new ViveDevice(m_cameraObject);

        _controllerManager = transform.Find("[CameraRig]").GetComponent<SteamVR_ControllerManager>();

        _viewgrid = transform.Find("ViewGrid").GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Update what items are being tracked
        bool needs_refresh = false;
        if (m_items.Length > 0 && m_items[0] != null)
        {
            if (_controllerManager.left != m_items[0].gameObject)
                needs_refresh = true;
            if (_controllerManager.objects.Length <= 0)
                _controllerManager.objects = new GameObject[2];
            _controllerManager.left = m_items[0].gameObject;
            _controllerManager.objects[0] = m_items[0].gameObject;
        }
        if (m_items.Length > 1 && m_items[1] != null)
        {
            if (_controllerManager.left != m_items[0].gameObject)
                needs_refresh = true;
            if (_controllerManager.objects.Length <= 1)
                _controllerManager.objects = new GameObject[2];
            _controllerManager.right = m_items[1].gameObject;
            _controllerManager.objects[1] = m_items[1].gameObject;
        }
        if (needs_refresh)
        {   // CHange made> Need to refresh the VR status
            _controllerManager.Refresh();
        }

        if (_headDevice.DiscoverController())
        {
            // When we lose tracking, turn off the motion of the player. We don't want any funky things happening here.
            if (_headDevice.Controller.connected && _headDevice.Controller.hasTracking)
            {
                _rigidbody.isKinematic = false;
            }
            else
            {
                _rigidbody.isKinematic = true;
            }
        }
        else
        {
            _rigidbody.isKinematic = true;
        }

        // Do viewgrid info
        if (_rigidbody.isKinematic)
        {
            _viewgridStrength = 1.0F;
        }
        else if (_rigidbody.velocity.magnitude > 0.1F && _rigidbody.velocity.magnitude < 1.0F)
        {
            if (_viewgridStrength < 1.0F - _rigidbody.velocity.magnitude)
            {
                _viewgridStrength = Mathf.Min(_viewgridStrength + Time.deltaTime * 4.0F, 1.0F - _rigidbody.velocity.magnitude);
            }
        }
        else if (_rigidbody.angularVelocity.magnitude > 5.0F && _rigidbody.angularVelocity.magnitude < 50.0F)
        {
            _viewgridStrength = Mathf.Max(1.0F, _viewgridStrength + Time.deltaTime);
        }
        else
        {
            _viewgridStrength -= Time.deltaTime;
        }
        _viewgridStrength = Mathf.Clamp(_viewgridStrength, 0.0F, 2.0F);
        if (_viewgrid !=null)
        {
            _viewgrid.material.color = new Color(1, 1, 1, Mathf.Min(_viewgridStrength,1.0F) * 0.0625F);
        }
    }
}