﻿using UnityEngine;
using System.Collections;

public class TestUI : MonoBehaviour
{
    public TestPlayer m_player;
    public Camera m_camera;

    public GameObject m_uiTextPrefab;

    public ItemHandyBase[] m_handItems;
    struct RadialMenu_t
    {
        public Transform parent;
        public Transform[] elements;
        public Transform cursor;
        public TextMesh text_info;
        public TextMesh[] element_text_info;
    }
    RadialMenu_t[] _handRadialMenus = null;
    enum RadialSelection
    {
        None = -1,
        Up = 0,
        Left = 1,
        Down = 2,
        Right = 3,
    }
    RadialSelection[] _handRadialSelection;

    // UI Positioning
    Quaternion _storedMotionOffset = Quaternion.identity;
    Quaternion _lastCameraRotation = Quaternion.identity;
    EHS.Element.BlenderQ _blenderStoredOffset2;
    EHS.Element.BlenderQ _blenderStoredOffset3;
    EHS.Element.BlenderQ _blenderStoredOffset4;
    Quaternion _current_offset;

    // Use this for initialization
    void Start()
    {
        // Set up the blenders
        {
            //_blenderStoredOffset.m_end = Quaternion.identity;
            _blenderStoredOffset2.m_end = Quaternion.identity;
            _blenderStoredOffset3.m_end = Quaternion.identity;
            _blenderStoredOffset4.m_end = Quaternion.identity;
        }
    }

    void Update()
    {
        UpdateRadialMenus();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = m_camera.transform.position;

        // Find the change in angle
        Quaternion delta = Quaternion.Inverse(m_camera.transform.rotation) * _lastCameraRotation;
        _lastCameraRotation = m_camera.transform.rotation;

        // Save offset
        _storedMotionOffset = delta;

        // Second pass smooth (slowly pull to stored rotation)
        _blenderStoredOffset2.m_end = _storedMotionOffset;
        _blenderStoredOffset2.SmoothDamp(0.08F);
        // Third pass smooth (quickly pull to the noisy slow)
        _blenderStoredOffset3.m_end = _blenderStoredOffset2.m_current;
        _blenderStoredOffset3.SmoothDamp(0.3F);
        // Fourth pass smooth (quickly pull to the noisy slow, but faster for spring)
        _blenderStoredOffset4.m_end = _blenderStoredOffset2.m_current;
        _blenderStoredOffset4.SmoothDamp(1.2F);

        //_current_offset *= delta;
        Quaternion rotation_offset = _blenderStoredOffset3.m_current * Quaternion.Inverse(_blenderStoredOffset4.m_current);

        // Pull out euler angles of the rotation
        Vector3 euler_offset = Quaternion.Inverse(rotation_offset).eulerAngles;
        euler_offset.x = Mathf.Repeat(euler_offset.x + 90.0F, 180F) - 90.0F;
        euler_offset.y = Mathf.Repeat(euler_offset.y + 90.0F, 180F) - 90.0F;
        euler_offset.z = Mathf.Repeat(euler_offset.z + 90.0F, 180F) - 90.0F;
        euler_offset *= -10.0F;

        transform.rotation = m_camera.transform.rotation * Quaternion.Euler(euler_offset);
    }

    void CreateRadialMenus()
    {
        _handRadialMenus = new RadialMenu_t[2];
        _handRadialMenus[0].parent = transform.Find("radial_left");
        _handRadialMenus[1].parent = transform.Find("radial_right");
        for (int i = 0; i < 2; ++i)
        {
            _handRadialMenus[i].elements = new Transform[4];
            _handRadialMenus[i].elements[(int)RadialSelection.Left] = _handRadialMenus[i].parent.Find("left");
            _handRadialMenus[i].elements[(int)RadialSelection.Right] = _handRadialMenus[i].parent.Find("right");
            _handRadialMenus[i].elements[(int)RadialSelection.Up] = _handRadialMenus[i].parent.Find("up");
            _handRadialMenus[i].elements[(int)RadialSelection.Down] = _handRadialMenus[i].parent.Find("down");
            _handRadialMenus[i].cursor = _handRadialMenus[i].parent.Find("cursor");

            // Create UI text info for radial selector
            GameObject text_info_gameobject = GameObject.Instantiate(m_uiTextPrefab);
            _handRadialMenus[i].text_info = text_info_gameobject.GetComponent<TextMesh>();
            _handRadialMenus[i].text_info.transform.parent = _handRadialMenus[i].parent;
            _handRadialMenus[i].text_info.transform.localPosition = new Vector3(i == 0 ? 1.0F : -1.0F, -1.0F, -1.5F);
            _handRadialMenus[i].text_info.transform.localScale = Vector3.one * 0.10F;
            _handRadialMenus[i].text_info.alignment = i == 0 ? TextAlignment.Left : TextAlignment.Right;
            _handRadialMenus[i].text_info.anchor = i == 0 ? TextAnchor.MiddleLeft : TextAnchor.MiddleRight;

            // Create text on each target
            _handRadialMenus[i].element_text_info = new TextMesh[4];
            for (int n = 0; n < 4; ++n)
            {
                GameObject mode_info_gameobject = GameObject.Instantiate(m_uiTextPrefab);
                mode_info_gameobject.transform.parent = _handRadialMenus[i].elements[n];
                mode_info_gameobject.transform.localPosition = new Vector3(0, 8, 1);
                mode_info_gameobject.transform.localRotation = Quaternion.Euler(180*n, 180*(n-1), 90 * n);
                mode_info_gameobject.transform.localScale = Vector3.one * 0.75F;
                _handRadialMenus[i].element_text_info[n] = mode_info_gameobject.GetComponent<TextMesh>();
            }

            // Make them a little smaller
            _handRadialMenus[i].parent.transform.localScale = _handRadialMenus[i].parent.transform.localScale * 0.85F;
        }
        _handRadialSelection = new RadialSelection[2];
    }

    void UpdateRadialMenus()
    {
        if (_handRadialMenus == null)
        {
            CreateRadialMenus();
        }

        for (int i = 0; i < 2; ++i)
        {
            if (m_handItems[i] == null) continue;

            ViveDevice device = m_handItems[i].Device;
            if (device != null && device.DiscoverController())
            {
                Vector2 trackPosition = device.Controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
                SetRadialXY(_handRadialMenus[i].cursor, trackPosition);

                // Perform lazy selection (no click, only drag)
                if ( trackPosition.magnitude > 0.5F )
                {
                    if ( Mathf.Abs(trackPosition.x) > Mathf.Abs(trackPosition.y) )
                    {   // We chose left or right
                        if (trackPosition.x > 0) _handRadialSelection[i] = RadialSelection.Right;
                        else _handRadialSelection[i] = RadialSelection.Left;
                    }
                    else
                    {   // We chose top or bottom
                        if (trackPosition.y > 0) _handRadialSelection[i] = RadialSelection.Up;
                        else _handRadialSelection[i] = RadialSelection.Down;
                    }
                    // Here we actually set new mode
                    m_handItems[i].SetMode((int)_handRadialSelection[i]);
                }

                // Set positions of radial elements
                for (int n = 0; n < 4; ++n)
                {
                    _handRadialMenus[i].elements[n].localScale = new Vector3( 1,1,4 ) * 0.07F;
                    if ( n == (int)_handRadialSelection[i])
                    {
                        MoveRadialXY(_handRadialMenus[i].elements[n], Vector2.zero, 8.0F * Time.deltaTime);
                        MoveRadialZ(_handRadialMenus[i].elements[n], 0.4F, 8.0F * Time.deltaTime);
                    }
                    else
                    {
                        MoveRadialXY(_handRadialMenus[i].elements[n], Quaternion.Euler(0,0,n*90) * Vector2.up * 0.6F, 8.0F * Time.deltaTime);
                        MoveRadialZ(_handRadialMenus[i].elements[n], -0.1F, 8.0F * Time.deltaTime);
                    }
                    // Set text of the radial element (replace with image someday)
                    if (m_handItems[i].GetModeEnabled(n))
                    {
                        _handRadialMenus[i].element_text_info[n].text = m_handItems[i].GetModeNameShort(n);
                    }
                    else
                    {
                        _handRadialMenus[i].element_text_info[n].text = "";
                    }
                }

                // Set descriptor text
                if (m_handItems[i].GetModeEnabled((int)_handRadialSelection[i]))
                {
                    _handRadialMenus[i].text_info.text = m_handItems[i].GetModeName((int)_handRadialSelection[i]);
                }
                else
                {
                    _handRadialMenus[i].text_info.text = "";
                }
            }
        }
    }
    void SetRadialXY(Transform transform_to_move, Vector2 position)
    {
        transform_to_move.localPosition = new Vector3(
            position.x,
            transform_to_move.localPosition.y,
            position.y);
    }
    void MoveRadialXY(Transform transform_to_move, Vector2 position, float speed)
    {
        Vector3 current_position = transform_to_move.localPosition;
        Vector3 target_position = new Vector3(
            position.x,
            current_position.y,
            position.y);

        // Create info to motion towards target
        Vector3 delta = target_position - current_position;
        float magnitude = delta.magnitude;

        // Check the motion to add
        if ( magnitude > speed )
        {   // Add normalized vector * speed
            current_position += delta / magnitude * speed;
        }
        else
        {   // We're at the target
            current_position = target_position;
        }

        transform_to_move.localPosition = current_position;
    }
    void MoveRadialZ(Transform transform_to_move, float position, float speed)
    {
        Vector3 current_position = transform_to_move.localPosition;
        Vector3 target_position = new Vector3(
            current_position.x,
            position,
            current_position.z);

        // Create info to motion towards target
        Vector3 delta = target_position - current_position;
        float magnitude = delta.magnitude;

        // Check the motion to add
        if (magnitude > speed)
        {   // Add normalized vector * speed
            current_position += delta / magnitude * speed;
        }
        else
        {   // We're at the target
            current_position = target_position;
        }

        transform_to_move.localPosition = current_position;
    }
}