﻿using UnityEngine;
using System.Collections;

public class TestPlayerCollider : MonoBehaviour
{
    public Transform m_headTransform;
    CapsuleCollider _collider;

    void Start()
    {
        _collider = GetComponent<CapsuleCollider>();
    }

    void FixedUpdate()
    {
        Vector3 head_pos = m_headTransform.localPosition;

        _collider.height = head_pos.y;
        Vector3 placement_pos = head_pos;
        placement_pos.y -= _collider.height * 0.5F - _collider.radius;
        transform.localPosition = placement_pos; 

    }
}