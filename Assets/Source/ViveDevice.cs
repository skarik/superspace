﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used to lazily manage Vive controller devices to prevent repeated code
/// </summary>
public class ViveDevice
{
    /// <summary>
    /// Explicit creation of the class.
    /// </summary>
    /// <param name="n_owner">Using Monobehavior with the SteamVR_TrackedObject</param>
    public ViveDevice (MonoBehaviour n_owner )
    {
        _owner = n_owner;
    }


    MonoBehaviour _owner;
    public MonoBehaviour Owner
    {
        get { return _owner; }
    }

    SteamVR_Controller.Device _controller;
    public SteamVR_Controller.Device Controller
    {
        get { return _controller; }
    }
    SteamVR_TrackedObject _vr_tracked_object;

    public bool DiscoverController()
    {
        // Would really like the tracked object so can grab correct controller to use input from
        if (_vr_tracked_object == null)
        {
            _vr_tracked_object = _owner.GetComponent<SteamVR_TrackedObject>();
        }
        // If the indices of the tracked controller change, we need to change the controller used for input
        if (_controller != null && _controller.index != (int)_vr_tracked_object.index)
        {   // So set it to invalid.
            _controller = null;
        }
        // If invalid controller...
        if (_controller == null)
        {
            // Pull device of the associated controller
            int deviceIndex;
            if (_vr_tracked_object == null)
            {
                deviceIndex = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.First);
            }
            else
            {
                deviceIndex = (int)_vr_tracked_object.index;
            }
            // Pull the controller object
            if (deviceIndex != -1)
            {
                Debug.Log("Controller on index: " + deviceIndex);
                _controller = SteamVR_Controller.Input(deviceIndex);
            }
        }

        if ( _controller != null)
        {
            if ( _controller.outOfRange == false )
                return _controller.valid && _controller.connected && _controller.hasTracking;
        }
        return false;
    }
}
