﻿using UnityEngine;
using System.Collections;

namespace EHS.Element
{
    //===============================================================================================//
    // Default smooth-damp blender utility
    //===============================================================================================//

    /// <summary>
    /// Blender structure, used to blend values
    /// </summary>
    public struct Blender
    {
        public float m_current;
        //public float m_start;
        public float m_end;
        private float _velocity;

        public float End { set { m_end = value; } }
        public float Current { set { m_current = value; } get { return m_current; } }

        public float SmoothDamp(float smoothTime)
        {
            m_current = Mathf.SmoothDamp(m_current, m_end, ref _velocity, smoothTime);
            return m_current;
        }
    };

    /// <summary>
    /// 3D Blender structure, used to blend vectors
    /// </summary>
    public struct Blender3
    {
        public Vector3 m_current;
        //public Vector3 m_start;
        public Vector3 m_end;
        private Vector3 _velocity;

        public Vector3 SmoothDamp(float smoothTime)
        {
            m_current = Vector3.SmoothDamp(m_current, m_end, ref _velocity, smoothTime);
            return m_current;
        }
        public Vector3 SmoothDampAngle(float smoothTime)
        {
            m_current.x = Mathf.SmoothDampAngle(m_current.x, m_end.x, ref _velocity.x, smoothTime);
            m_current.y = Mathf.SmoothDampAngle(m_current.y, m_end.y, ref _velocity.y, smoothTime);
            m_current.z = Mathf.SmoothDampAngle(m_current.z, m_end.z, ref _velocity.z, smoothTime);
            return m_current;
        }
    };

    /// <summary>
    /// Blender structure, used to blend quaternions.
    /// Has issues with components, so does not actually work properly (but has cool results).
    /// </summary>
    public struct BlenderQ
    {
        public Quaternion m_current;
        //public Quaternion m_start;
        public Quaternion m_end;
        private Vector4 _velocity;

        public Quaternion SmoothDamp(float smoothTime)
        {
            m_current.x = Mathf.SmoothDamp(m_current.x, m_end.x, ref _velocity.x, smoothTime);
            m_current.y = Mathf.SmoothDamp(m_current.y, m_end.y, ref _velocity.y, smoothTime);
            m_current.z = Mathf.SmoothDamp(m_current.z, m_end.z, ref _velocity.z, smoothTime);
            m_current.w = Mathf.SmoothDamp(m_current.w, m_end.w, ref _velocity.w, smoothTime);
            return m_current;
        }
        public Quaternion SmoothDampExperimental(float smoothTime)
        {
            m_current.x = Mathf.SmoothDamp(m_current.x, m_end.x, ref _velocity.x, smoothTime);
            m_current.y = Mathf.SmoothDamp(m_current.y, m_end.y, ref _velocity.y, smoothTime);
            m_current.z = Mathf.SmoothDamp(m_current.z, m_end.z, ref _velocity.z, smoothTime);
            m_current.w = Mathf.SmoothDamp(m_current.w, m_end.w, ref _velocity.w, smoothTime * 0.5f);
            return m_current;
        }
    };

    //===============================================================================================//
    // Lowpass blender utility
    //===============================================================================================//

    /// <summary>
    /// Lowpass blender structure, used to pull stuttering out of blend results.
    /// </summary>
    public struct BlenderLowpass3
    {
        private float[] _blends;
        private Vector3[] _values;

        public void Setup(float[] blendParameters)
        {
            _blends = blendParameters;
            _values = new Vector3[blendParameters.Length];
        }

        public Vector3 SmoothDamp(Vector3 input)
        {
            // Push the old blended values up the list
            for (int i = _values.Length - 1; i > 0; --i)
            {
                _values[i] = _values[i - 1];
            }

            // Blend the input into the system
            _values[0] = Vector3.Lerp(_values[0], input, _blends[0]);

            // Blend across all the old values
            for (int i = 1; i < _blends.Length; ++i)
            {
                _values[0] = Vector3.Lerp(_values[0], _values[i], _blends[i]);
            }

            // Return the 0th value
            return _values[0];
        }
    }

    /// <summary>
    /// Lowpass blender structure, used to pull stuttering out of blend results.
    /// </summary>
    public struct BlenderLowpassQ
    {
        private float[] _blends;
        private Quaternion[] _values;

        public void Setup(float[] blendParameters)
        {
            _blends = blendParameters;
            _values = new Quaternion[blendParameters.Length];
        }

        public Quaternion SmoothDamp(Quaternion input)
        {
            // Push the old blended values up the list
            for (int i = _values.Length - 1; i > 0; --i)
            {
                _values[i] = _values[i - 1];
            }

            // Blend the input into the system
            _values[0] = Quaternion.Lerp(_values[0], input, _blends[0]);

            // Blend across all the old values
            for (int i = 1; i < _blends.Length; ++i)
            {
                _values[0] = Quaternion.Lerp(_values[0], _values[i], _blends[i]);
            }

            // Return the 0th value
            return _values[0];
        }
    }

}