﻿using UnityEngine;
using System.Collections;

namespace EHS.Math
{
    public struct BoundsI
    {
        //=========================================//

        public int minx;
        public int miny;
        public int minz;
        public int maxx;
        public int maxy;
        public int maxz;

        //=========================================//

        public BoundsI(int x, int y, int z)
        {
            minx = x; miny = y; minz = z;
            maxx = x; maxy = y; maxz = z;
        }

        //=========================================//

        public void ExtendTo(int x, int y, int z)
        {
            if (x < minx) minx = x;
            if (y < miny) miny = y;
            if (z < minz) minz = z;
            if (x > maxx) maxx = x;
            if (y > maxy) maxy = y;
            if (z > maxz) maxz = z;
        }

        public void Expand(int border)
        {
            minx -= border;
            miny -= border;
            minz -= border;
            maxx += border;
            maxy += border;
            maxz += border;
        }
    }
}