﻿#if MONO_ASSEMBLY

using UnityEngine;
using Mono.Simd;

namespace EHS.Math
{
    public class SIMD : MonoBehaviour
    {
        public class RuntimeDetection
        {
            public readonly bool basic_support;
            public readonly bool enhanced_support;

            public RuntimeDetection()
            {
                basic_support = MinimalSimdSupport();
                enhanced_support = EnhancedSimdSupport();
            }

            private bool MinimalSimdSupport()
            {   
                return SimdRuntime.IsMethodAccelerated(typeof (Vector4f), "op_Addition") &&
                       SimdRuntime.IsMethodAccelerated(typeof (Vector4f), "op_Multiply") &&
                       SimdRuntime.IsMethodAccelerated(typeof (VectorOperations), "Shuffle", typeof (Vector4f),
                           typeof (ShuffleSel));
            }

            private bool EnhancedSimdSupport()
            {
                return
                    SimdRuntime.IsMethodAccelerated(typeof (VectorOperations), "HorizontalAdd", typeof (Vector4f),
                        typeof (Vector4f)) &&
                    SimdRuntime.IsMethodAccelerated(typeof (Vector4f), "op_Multiply");
            }
            
            public float DotProduct(ref Vector4f a, ref Vector4f b)
            {
                if (enhanced_support)
                {
                    Vector4f prod = a*b;
                    prod = prod.HorizontalAdd(prod);
                    prod = prod.HorizontalAdd(prod);
                    return prod.X;
                }
                else if (basic_support)
                {
                    Vector4f prod = a*b;
                    prod += prod.Shuffle(ShuffleSel.XFromZ | ShuffleSel.YFromW);
                    prod += prod.Shuffle(ShuffleSel.XFromY);
                    return prod.X;
                }
                else
                {
                    return a.X*b.X + a.Y*b.Y + a.Z*b.Z + a.W*b.W;
                }
            }

            public void Main()
            {
                Vector4f x = new Vector4f(1, 2, 3, 1);
                Vector4f y = new Vector4f(5, 6, 7, 1);
                float res = DotProduct(ref x, ref y);
                UnityEngine.Debug.Log(string.Format("Dot product of {0} . {1} is {2}", x, y, res));
            }
        }
    }
}

#endif