﻿using UnityEngine;
using System.Collections;

namespace EHS.Math
{
    public struct Vector3i
    {
        //=========================================//

        public int x;
        public int y;
        public int z;

        //=========================================//

        public Vector3i(int _x, int _y, int _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
        public Vector3i(Vector3 vect)
        {
            x = (int)vect.x;
            y = (int)vect.y;
            z = (int)vect.z;
        }

        public static explicit operator Vector3i(Vector3 vect)  // explicit byte to digit conversion operator
        {
            Vector3i v = new Vector3i(vect);
            return v;
        }
    }
}