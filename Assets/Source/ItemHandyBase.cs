﻿using UnityEngine;
using System.Collections;

public class ItemHandyBase : MonoBehaviour
{
    protected Rigidbody _affectedBody;
    public Rigidbody AffectedBody
    {
        set { _affectedBody = value; }
    }
    protected ViveDevice _device;
    public ViveDevice Device
    {
        get { return _device; }
    }

    protected void Start()
    {
        _device = new ViveDevice(this);
    }

    // HAPTIC FEEDBACK FUNCTIONALITY

    private float _hapticPulseTimer = 0.0F;
    private float _hapticPulseSpeed = 0.0F;
    private ushort _hapticPulseStrength = 0;

    private float _hapticPulseSpeedDecay = 10.0F;

    protected void Update()
    {
        if (_device.DiscoverController())
        {
            // Need some specific feels to the pulsing
            if (_hapticPulseStrength > 0 && _hapticPulseSpeed > 0.0F)
            {
                _hapticPulseTimer -= _hapticPulseSpeed * Time.deltaTime;
                if (_hapticPulseTimer < 0.0F)
                {
                    _hapticPulseTimer = Mathf.Max(0.0F, _hapticPulseTimer + 0.1F);
                    _device.Controller.TriggerHapticPulse(_hapticPulseStrength);//,Valve.VR.EVRButtonId.k_EButton_Grip);
                }
                _hapticPulseSpeed -= _hapticPulseSpeedDecay * Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="strength">Value between 500 and 4000</param>
    /// <param name="speed">10x the time between sucessive ticks</param>
    public void TriggerHapticFeedback(float strength, float speed)
    {
        if (strength > 0)
            _hapticPulseStrength = (ushort)strength;
        if (speed > 0)
            _hapticPulseSpeed = speed;
    }

    // ITEM UI PROPERTIES

    public virtual string GetModeNameShort ( int mode )
    {
        return "???";
    }
    public virtual string GetModeName ( int mode )
    {
        return "Unknown";
    }
    public virtual bool GetModeEnabled ( int mode )
    {
        return false;
    }
    public virtual void SetMode ( int mode )
    {
        ; // Nothing
    }
}